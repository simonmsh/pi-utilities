#include <string>

class TemperatureReader
{
	public:
		TemperatureReader();
		std::string readTemperature() const;
		double readTemperatureDouble() const;
		int readTemperatureInt() const;
	private:
};

