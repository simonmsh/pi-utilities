#include "WriteToWebserver.h"
#include "TemperatureReader.h"
#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <csignal>

bool running = true;


void exitHandler(int s)
{
	std::cout << "Detected Ctrl-C signal number " << s <<  std::endl;
	running = false;
}

int main() {
	std::signal(SIGINT, exitHandler);
	std::string line;
	const std::string outputFileName = "//var//www//html//thermometer//cpu1.txt";   

	// Read the CPU temperature every 10 seconds, and write it to a file that can be served by Apache. Use exponetial smoothing.
	auto webserverWriter = std::make_unique<WriteToWebserver>();
	auto temperatureReader = std::make_unique<TemperatureReader>();
	double alpha = 0.2; //Smoothing parameter
	int smoothed = std::numeric_limits<int>::min();
	while (running) {
		int latest = temperatureReader->readTemperatureInt();
		if (smoothed == std::numeric_limits<int>::min()) 
		{
			smoothed = latest;
		}
		else
		{
			smoothed = static_cast<int>(static_cast<double>(smoothed * (1.0 - alpha)) + (alpha * static_cast<double>(latest)));
		} 
		std::cout  << latest << ", " << smoothed << std::endl;
		
		webserverWriter->writeString(std::to_string(smoothed), outputFileName);
		std::this_thread::sleep_for(std::chrono::milliseconds(10000)); //Sleep for 10 seconds
	}
	std::cout  << "Exiting" << std::endl;
	return 0;
}
