#include "TemperatureReader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

	TemperatureReader::TemperatureReader()
	{
	}

	double TemperatureReader::readTemperatureDouble() const
	{
		return std::stod(readTemperature())/1000.0;
	}

	int TemperatureReader::readTemperatureInt() const
	{
		return std::stoi(readTemperature());
	}

	std::string TemperatureReader::readTemperature() const
	{
		std::vector<std::string> vecOfStr;
		const std::string inputFileName = "//sys//class//thermal//thermal_zone0//temp"; //Get CPU temperature

		std::ifstream in(inputFileName);
		if(!in)
		{
			std::cerr << "Cannot open the File : "<<inputFileName<<std::endl;
			return "";
		}
	     
		std::string str;
		while (std::getline(in, str))
		{
			if(str.size() > 0)
				vecOfStr.push_back(str);
		}
		in.close();
		std::string line = vecOfStr.front();
		return line;
	}	
