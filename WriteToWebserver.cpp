#include "WriteToWebserver.h"
#include <iostream>
#include <fstream>

	WriteToWebserver::WriteToWebserver()
	{
	}

	void WriteToWebserver::writeString(const std::string& line, const std::string& fileName) const
	{
		//Write data to file that is served by Apache
		std::ofstream outputFile;
		outputFile.open (fileName);
		if (outputFile.is_open()) 
		{
			outputFile << line << std::endl;
		}
		else
		{
			std::cerr << "Unable to open output file" << std::endl;				
		}
		outputFile.close();
	}	

